关于HelloChrome
-------------
>HelloChrome 相当一个编写Chrome插件的HelloWorld小范例   
可以通过该小例速度了解Chrome开发及配置,亦可了解后自行开发简单的Chrome扩展.   
   
关于Chrome插件
-------------
可能是因为Chrome官网以及Chrome商店都被墙的原因，没有太多的人关注Chrome扩展的开发。   
但是Chrome扩展着实强大。   
所以写了一个HelloWorld供大家参考。   
   
本小例并非直接将官网教程翻译搬运过来，将其进行了简化。   
简明扼要的主要说明了如何配置，如何开发，以及开必主要用到的broswer_action, page_action, content_script。   
简单直接粗暴，本人不喜欢把东西吃透了再用，边用边吃，用会了也就吃透了。   
   
如果JS底子不太差，有本例就应该能开发简单的扩展应用了。   
Chrome扩展开发的API数量较大，功能较多，如果需要开发强大的功能调用浏览器的API还请移步到Chrome官网查询。   
在此附上官网API地址:[https://developer.chrome.com/extensions/api_index](https://developer.chrome.com/extensions/api_index) （需要翻墙）   
   
另外附上我写的详细的HelloChrome开发过程博文一篇[15分钟学会写chrome插件](http://my.oschina.net/isnail/blog/371409)   
   
实现的功能
-------------
本示例主要展示了broswer_action, content_script的使用。   
可以简单的理解为一个是外部JS的运行，一个是注入到页面里的JS的运行。   
运行效果如下图所示，图片来自上链博文，可移步至博文观察。   
![browser_action](http://static.oschina.net/uploads/space/2015/0124/195455_ObYM_1049249.png)   
![content_script](http://static.oschina.net/uploads/space/2015/0124/195637_ycut_1049249.png)   
